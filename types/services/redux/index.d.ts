import React from 'react';
import { Access, Telescope, User } from '../types/index.js';
interface Props {
  children?: React.ReactNode;
}
export declare function StoreProvider({ children }: Props): React.JSX.Element;
export declare const storageObject: {
  useStore(): {
    user: User | null;
    telescopeAccess: (inValue: string) => boolean | undefined;
    isDeveloper: boolean | '' | null;
    clearUser: () => {
      payload: undefined;
      type: 'user/clear';
    };
    updateUser: (user: User) => {
      payload: User | null;
      type: 'user/update';
    };
    access: Access | null;
    clearAccess: () => {
      payload: undefined;
      type: 'access/clear';
    };
    updateAccess: (access: Access) => {
      payload: Access | null;
      type: 'access/update';
    };
    application: import('../types/application.js').ApplicationState;
    clearApp: () => {
      payload: undefined;
      type: 'Application/clear';
    };
    updateAppContent1: (content: object) => {
      payload: object;
      type: 'Application/setContent1';
    };
    updateAppContent2: (content: object) => {
      payload: object;
      type: 'Application/setContent2';
    };
    updateAppContent3: (content: object) => {
      payload: object;
      type: 'Application/setContent3';
    };
    updateAppContent4: (content: object) => {
      payload: object;
      type: 'Application/setContent4';
    };
    updateAppContent5: (content: object) => {
      payload: object;
      type: 'Application/setContent5';
    };
    help: import('../types/help.js').HelpState;
    helpComponent: (content: object) => {
      payload: object;
      type: 'Help/setHelpComponent';
    };
    helpContent: (content: object) => {
      payload: object;
      type: 'Help/setHelpContent';
    };
    helpToggle: () => {
      payload: undefined;
      type: 'Help/setHelpToggle';
    };
    telescope: Telescope;
    updateTelescope: (telescope: Telescope) => {
      payload: Telescope;
      type: 'telescope/change';
    };
    themeMode: {
      mode: string;
    };
    darkMode: boolean;
    toggleTheme: () => {
      payload: undefined;
      type: 'theme/toggle';
    };
  };
};
export {};
