import { PayloadAction } from '@reduxjs/toolkit';
import { Access, AccessState } from '../../../types/index.js';
export declare const accessSlice: import('@reduxjs/toolkit').Slice<
  AccessState,
  {
    update: (
      state: import('immer').WritableDraft<AccessState>,
      action: PayloadAction<Access | null>,
    ) => void;
    clear: (state: import('immer').WritableDraft<AccessState>) => void;
  },
  'access',
  'access',
  import('@reduxjs/toolkit').SliceSelectors<AccessState>
>;
export declare const accessSliceActions: import('@reduxjs/toolkit').CaseReducerActions<
  {
    update: (
      state: import('immer').WritableDraft<AccessState>,
      action: PayloadAction<Access | null>,
    ) => void;
    clear: (state: import('immer').WritableDraft<AccessState>) => void;
  },
  'access'
>;
export declare const accessSliceReducer: import('redux').Reducer<AccessState>;
declare const _default: import('redux').Reducer<AccessState>;
export default _default;
