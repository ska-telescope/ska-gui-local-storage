export declare const themeSlice: import('@reduxjs/toolkit').Slice<
  {
    mode: string;
  },
  {
    toggle: (
      state: import('immer').WritableDraft<{
        mode: string;
      }>,
    ) => void;
  },
  'theme',
  'theme',
  import('@reduxjs/toolkit').SliceSelectors<{
    mode: string;
  }>
>;
export declare const themeSliceActions: import('@reduxjs/toolkit').CaseReducerActions<
  {
    toggle: (
      state: import('immer').WritableDraft<{
        mode: string;
      }>,
    ) => void;
  },
  'theme'
>;
export declare const themeSliceReducer: import('redux').Reducer<{
  mode: string;
}>;
declare const _default: import('redux').Reducer<{
  mode: string;
}>;
export default _default;
