import { PayloadAction } from '@reduxjs/toolkit';
import { User, UserState } from '../../../types/index.js';
export declare const initialState: UserState;
export declare const userSlice: import('@reduxjs/toolkit').Slice<
  UserState,
  {
    update: (
      state: import('immer').WritableDraft<UserState>,
      action: PayloadAction<User | null>,
    ) => void;
    clear: (state: import('immer').WritableDraft<UserState>) => void;
  },
  'user',
  'user',
  import('@reduxjs/toolkit').SliceSelectors<UserState>
>;
export declare const userSliceActions: import('@reduxjs/toolkit').CaseReducerActions<
  {
    update: (
      state: import('immer').WritableDraft<UserState>,
      action: PayloadAction<User | null>,
    ) => void;
    clear: (state: import('immer').WritableDraft<UserState>) => void;
  },
  'user'
>;
export declare const userSliceReducer: import('redux').Reducer<UserState>;
declare const _default: import('redux').Reducer<UserState>;
export default _default;
