import { PayloadAction } from '@reduxjs/toolkit';
import { ApplicationState } from '../../../types/index.js';
export declare const applicationSlice: import('@reduxjs/toolkit').Slice<
  ApplicationState,
  {
    clear: (state: import('immer').WritableDraft<ApplicationState>) => void;
    setContent1: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent2: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent3: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent4: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent5: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
  },
  'Application',
  'Application',
  import('@reduxjs/toolkit').SliceSelectors<ApplicationState>
>;
export declare const applicationSliceActions: import('@reduxjs/toolkit').CaseReducerActions<
  {
    clear: (state: import('immer').WritableDraft<ApplicationState>) => void;
    setContent1: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent2: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent3: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent4: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
    setContent5: (
      state: import('immer').WritableDraft<ApplicationState>,
      action: PayloadAction<object>,
    ) => void;
  },
  'Application'
>;
export declare const applicationSliceReducer: import('redux').Reducer<ApplicationState>;
declare const _default: import('redux').Reducer<ApplicationState>;
export default _default;
