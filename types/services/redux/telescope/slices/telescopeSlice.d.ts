import { PayloadAction } from '@reduxjs/toolkit';
import { Telescope, TelescopeState } from '../../../types/index.js';
export declare const telescopeSlice: import('@reduxjs/toolkit').Slice<
  TelescopeState,
  {
    change: (
      state: import('immer').WritableDraft<TelescopeState>,
      action: PayloadAction<Telescope>,
    ) => void;
  },
  'telescope',
  'telescope',
  import('@reduxjs/toolkit').SliceSelectors<TelescopeState>
>;
export declare const telescopeSliceActions: import('@reduxjs/toolkit').CaseReducerActions<
  {
    change: (
      state: import('immer').WritableDraft<TelescopeState>,
      action: PayloadAction<Telescope>,
    ) => void;
  },
  'telescope'
>;
export declare const telescopeSliceReducer: import('redux').Reducer<TelescopeState>;
declare const _default: import('redux').Reducer<TelescopeState>;
export default _default;
