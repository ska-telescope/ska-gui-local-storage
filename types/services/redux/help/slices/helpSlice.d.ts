import { PayloadAction } from '@reduxjs/toolkit';
import { HelpState } from '../../../types/index.js';
export declare const helpSlice: import('@reduxjs/toolkit').Slice<
  HelpState,
  {
    setHelpComponent: (
      state: import('immer').WritableDraft<HelpState>,
      action: PayloadAction<object>,
    ) => void;
    setHelpContent: (
      state: import('immer').WritableDraft<HelpState>,
      action: PayloadAction<object>,
    ) => void;
    setHelpToggle: (state: import('immer').WritableDraft<HelpState>) => void;
  },
  'Help',
  'Help',
  import('@reduxjs/toolkit').SliceSelectors<HelpState>
>;
export declare const helpSliceActions: import('@reduxjs/toolkit').CaseReducerActions<
  {
    setHelpComponent: (
      state: import('immer').WritableDraft<HelpState>,
      action: PayloadAction<object>,
    ) => void;
    setHelpContent: (
      state: import('immer').WritableDraft<HelpState>,
      action: PayloadAction<object>,
    ) => void;
    setHelpToggle: (state: import('immer').WritableDraft<HelpState>) => void;
  },
  'Help'
>;
export declare const helpSliceReducer: import('redux').Reducer<HelpState>;
declare const _default: import('redux').Reducer<HelpState>;
export default _default;
