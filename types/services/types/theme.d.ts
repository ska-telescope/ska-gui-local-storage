export interface ThemeState {
  theme: Theme;
}
export type Theme = {
  mode: string;
};
