export type Telescope = {
  code: string;
  name: string;
  location: string;
  position: {
    lat: number;
    lon: number;
  };
  image: string;
};
export interface TelescopeState {
  telescope: Telescope | null;
}
export declare let telescope: Telescope;
