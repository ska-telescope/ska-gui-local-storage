import { TypedUseSelectorHook } from 'react-redux';
import { AccessState } from './access.js';
import { TelescopeState } from './telescope.js';
import { ThemeState } from './theme.js';
import { UserState } from './user.js';
export interface AccessReduxState {
  access: AccessState;
}
export interface TelescopeReduxState {
  telescope: TelescopeState;
}
export interface ThemeReduxState {
  theme: ThemeState;
}
export interface UserReduxState {
  user: UserState;
}
export type UseAccessSelectorHook = TypedUseSelectorHook<AccessReduxState>;
export type UseTelescopeSelectorHook = TypedUseSelectorHook<TelescopeReduxState>;
export type UseThemeSelectorHook = TypedUseSelectorHook<ThemeReduxState>;
export type UseUserSelectorHook = TypedUseSelectorHook<UserReduxState>;
