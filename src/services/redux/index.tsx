import React from 'react';
import { configureStore } from '@reduxjs/toolkit';
import { Provider, useSelector, useDispatch } from 'react-redux';

import { TELESCOPE_LOW, THEME_DARK } from '@ska-telescope/ska-javascript-components';

import { Access, Telescope, User } from '../types/index.js';
import { accessSliceActions, accessSliceReducer } from './access/index.js';
import { helpSliceActions, helpSliceReducer } from './help/index.js';
import { telescopeSliceActions, telescopeSliceReducer } from './telescope/index.js';
import { themeSliceActions, themeSliceReducer } from './theme/index.js';
import { userSliceActions, userSliceReducer } from './user/index.js';
import { applicationSliceActions, applicationSliceReducer } from './application/index.js';

const DEV = 'developer';

const rootReducer = {
  access: accessSliceReducer,
  application: applicationSliceReducer,
  help: helpSliceReducer,
  telescope: telescopeSliceReducer,
  themeMode: themeSliceReducer,
  user: userSliceReducer,
};

const store = configureStore({
  reducer: rootReducer,
});

type RootState = ReturnType<typeof store.getState>;

interface Props {
  children?: React.ReactNode;
}

export function StoreProvider({ children }: Props) {
  return <Provider store={store}>{children}</Provider>;
}

export const storageObject = {
  useStore() {
    const access = useSelector((state: RootState) => state.access?.access);
    const application = useSelector((state: RootState) => state.application);
    const help = useSelector((state: RootState) => state.help);
    const user = useSelector((state: RootState) => state.user?.user);
    const telescopeAccess = (inValue: string) => {
      return access?.telescopes.includes(inValue);
    };
    const isDeveloper = user && user.username && user.username.indexOf(DEV) >= 0;

    const telescope = useSelector((state: RootState) =>
      state?.telescope?.telescope ? state.telescope.telescope : TELESCOPE_LOW,
    );
    const themeMode = useSelector((state: RootState) => state.themeMode);
    const darkMode = themeMode?.mode === THEME_DARK;

    const dispatch = useDispatch();
    return {
      user,
      telescopeAccess,
      isDeveloper,
      clearUser: () => dispatch(userSliceActions.clear()),
      updateUser: (user: User) => dispatch(userSliceActions.update(user)),
      //
      access,
      clearAccess: () => dispatch(accessSliceActions.clear()),
      updateAccess: (access: Access) => dispatch(accessSliceActions.update(access)),
      //
      application,
      clearApp: () => dispatch(applicationSliceActions.clear()),
      updateAppContent1: (content: object) =>
        dispatch(applicationSliceActions.setContent1(content)),
      updateAppContent2: (content: object) =>
        dispatch(applicationSliceActions.setContent2(content)),
      updateAppContent3: (content: object) =>
        dispatch(applicationSliceActions.setContent3(content)),
      updateAppContent4: (content: object) =>
        dispatch(applicationSliceActions.setContent4(content)),
      updateAppContent5: (content: object) =>
        dispatch(applicationSliceActions.setContent5(content)),
      //
      help,
      helpComponent: (content: object) => dispatch(helpSliceActions.setHelpComponent(content)),
      helpContent: (content: object) => dispatch(helpSliceActions.setHelpContent(content)),
      helpToggle: () => dispatch(helpSliceActions.setHelpToggle()),
      //
      telescope,
      updateTelescope: (telescope: Telescope) => dispatch(telescopeSliceActions.change(telescope)),
      //
      themeMode,
      darkMode,
      toggleTheme: () => dispatch(themeSliceActions.toggle()),
    };
  },
};
