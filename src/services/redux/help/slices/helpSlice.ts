import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { HelpState } from '../../../types/index.js';

const initialState: HelpState = {
  content: [],
  component: [],
  showHelp: false,
};

export const helpSlice = createSlice({
  name: 'Help',
  initialState,
  reducers: {
    setHelpComponent: (state, action: PayloadAction<object>) => {
      state.component = action.payload;
    },
    setHelpContent: (state, action: PayloadAction<object>) => {
      state.content = action.payload;
    },
    setHelpToggle: (state) => {
      state.showHelp = !state.showHelp;
    },
  },
});

export const helpSliceActions = helpSlice.actions;
export const helpSliceReducer = helpSlice.reducer;
export default helpSlice.reducer;
