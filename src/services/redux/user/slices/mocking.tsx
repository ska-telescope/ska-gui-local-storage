import { UserReduxState } from '../../../types/index.js';
export const mockUser: UserReduxState = {
  user: {
    user: {
      id: 'ID',
      username: 'Doc, John',
      email: 'dummy@gmail.com',
      language: 'en',
      token: '',
    },
  },
};
