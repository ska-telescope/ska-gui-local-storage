import { TelescopeReduxState } from '../../../types/index.js';
import { TELESCOPE_MID } from '@ska-telescope/ska-javascript-components';
export const mockTelescope: TelescopeReduxState = {
  telescope: {
    telescope: TELESCOPE_MID,
  },
};
