import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ApplicationState } from '../../../types/index.js';

const initialState: ApplicationState = {
  content1: [],
  content2: [],
  content3: [],
  content4: [],
  content5: [],
};

export const applicationSlice = createSlice({
  name: 'Application',
  initialState,
  reducers: {
    clear: (state) => {
      state.content1 = [];
      state.content2 = [];
      state.content3 = [];
      state.content4 = [];
      state.content5 = [];
    },
    setContent1: (state, action: PayloadAction<object>) => {
      state.content1 = action.payload;
    },
    setContent2: (state, action: PayloadAction<object>) => {
      state.content2 = action.payload;
    },
    setContent3: (state, action: PayloadAction<object>) => {
      state.content3 = action.payload;
    },
    setContent4: (state, action: PayloadAction<object>) => {
      state.content4 = action.payload;
    },
    setContent5: (state, action: PayloadAction<object>) => {
      state.content5 = action.payload;
    },
  },
});

export const applicationSliceActions = applicationSlice.actions;
export const applicationSliceReducer = applicationSlice.reducer;
export default applicationSlice.reducer;
