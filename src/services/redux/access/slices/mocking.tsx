import { AccessReduxState } from '../../../types/index.js';
import { TELESCOPE_LOW, TELESCOPE_MID } from '@ska-telescope/ska-javascript-components';
export const mockAccess: AccessReduxState = {
  access: {
    access: {
      telescopes: [TELESCOPE_LOW.code, TELESCOPE_MID.code],
      menu: {
        too: [{ title: 'Dummy Title', path: 'Dummy Path' }],
        top: [],
        dev: [],
        obs: [],
        res: [],
        def: [],
        lnk: [],
      },
    },
  },
};
