import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Access, AccessState } from '../../../types/index.js';

const initialState: AccessState = {
  access: null,
};

export const accessSlice = createSlice({
  name: 'access',
  initialState,
  reducers: {
    update: (state, action: PayloadAction<Access | null>) => {
      state.access = action.payload;
    },
    clear: (state) => {
      state.access = null;
    },
  },
});

export const accessSliceActions = accessSlice.actions;
export const accessSliceReducer = accessSlice.reducer;
export default accessSlice.reducer;
