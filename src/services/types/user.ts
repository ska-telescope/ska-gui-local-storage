export interface UserState {
  user: User | null;
}

export type User = {
  id: string;
  username: string;
  email?: string;
  language?: string;
  token?: string;
};
