export interface HelpState {
  content: object;
  component: object;
  showHelp: boolean;
}
