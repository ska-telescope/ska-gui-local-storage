export interface AccessState {
  access: Access | null;
}

export type Access = {
  telescopes: string[];
  menu: {
    def: { title: string; path: string }[];
    dev: { title: string; path: string }[];
    lnk: { title: string; path: string }[];
    obs: { title: string; path: string }[];
    res: { title: string; path: string }[];
    too: { title: string; path: string }[];
    top: { title: string; path: string }[];
  };
};
