Overview
~~~~~~~~

The `ska-gui-local-storage` repository contains the code for the SKA GUI Local Storage component that is used to store data used by front end applications in a Redux store.

Usage
=====

...