Documentation for the SKA GUI Local Storage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The `ska-gui-local-storage` repository contains the code for the SKA GUI Local Storage component.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview
   Development
   Version
   
