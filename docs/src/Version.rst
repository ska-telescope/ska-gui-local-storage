Version
~~~~~~~

*Overview*

This is a simple constant that provides the latest version of the library

.. admonition:: *Code snippet* ./services/theme.tsx
   
   import { GUI_LOCAL_STORAGE_VERSION } from '@ska-telescope/ska-gui-local-storage';
   
.. admonition:: Testing Identifier

   n/a