Changelog
==========

2.0.4
*****

Updated to ska-javascript-components v2.23

2.0.3
*****

Sorted out the vunerabilities of the vue-template-compiler package

2.0.2
*****

Changed to NodeNext

2.0.1
*****

Replaced babel with SWC